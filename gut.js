var iframes = [];
function updateColumns() {
    if (iframes.length == 0) return;
    var columnView = window.parent.document.getElementById('column-view');
    columnView.style.height = '0px';
    iframes.forEach((iframe, index) => {
        var delta = index * window.parent.document.documentElement.clientHeight;
        iframe.contentDocument.documentElement.scrollTop = delta;
    });

    window.parent.requestAnimationFrame(() => {
        var docElement = iframes[iframes.length - 1].contentDocument.documentElement;
        var delta = (iframes.length - 1) * window.parent.document.documentElement.clientHeight;
        columnView.style.height = (docElement.scrollHeight - delta) + 'px';
    });
}

function resizeOnInnerImg(doc) {
    doc.querySelectorAll('iframe[src$=".svg"]')
        .forEach((self) => {
            var img = doc.createElement('img');
            img.src = self.src;
            self.parentNode.insertBefore(img, self);
            img.addEventListener('load', () => {
                var originalHeight = img.clientHeight;
                img.style.width = '100%';
                self.style.height = Math.min(img.clientHeight, originalHeight, doc.documentElement.clientHeight - 215) + 'px';
                img.remove();
                window.parent.requestAnimationFrame(updateColumns);
            });
        });

    window.addEventListener('resize', () => {
        resizeOnInnerImg(doc);
    }, { once: true });
}

var srcdoc;
function columnChanged(event) {
    var columnView = window.parent.document.getElementById('column-view');
    if (!columnView) {
        window.parent.requestAnimationFrame(() => {
            columnChanged(event);
        });
        return;
    }
    for (var column of columnView.querySelectorAll('td'))
        column.remove();
    var count = event.target.value || event || 2;
    if (count == 1) {
        columnView.style.display = 'none';
        window.parent.document.getElementById('colcount').value = 1;
        window.parent.document.getElementsByTagName('main')[0].style.display = '';
        window.parent.document.getElementsByTagName('header')[0].style.display = '';
        resizeOnInnerImg(window.parent.document);
    }
    else {
        window.parent.document.getElementsByTagName('main')[0].style.display = 'none';
        window.parent.document.getElementsByTagName('header')[0].style.display = 'none';
        columnView.style.display = '';
        iframes = [];
        for (var i = 0; i < count; ++i) {
            var td = window.parent.document.createElement('td');
            columnView.querySelector('tr').appendChild(td);
            td.style.width = (100 / count) + '%';
            td.style.height = '100%';
            var iframe = td.appendChild(window.parent.document.createElement('iframe'));
            iframe.srcdoc = srcdoc;
            iframe.style.width = '100%';
            iframe.style.height = '100%';
            iframe.scrolling = 'no';
            iframes.push(iframe);
        }

        iframes.forEach((iframe) => {
            iframe.addEventListener('load', () => {
                var colcount = iframe.contentDocument.getElementById('colcount');
                colcount.value = count;
                colcount.addEventListener('change', columnChanged);
                iframe.contentDocument.documentElement.querySelectorAll('h1, h2, h3, h4, h5, h6')
                    .forEach((heading) => {
                        heading.addEventListener('click', (event) => {
                            document.documentElement.scrollTop = event.target.getBoundingClientRect().top - event.target.ownerDocument.documentElement.getBoundingClientRect().top;
                        });
                    });
                resizeOnInnerImg(iframe.contentDocument);
            });
        });
    }
}

function setup() {
    document.getElementById('doc').value =
        '<!doctype html>' + document.documentElement.outerHTML;
    var toctree = document.getElementById('toctree');
    if (toctree.src.endsWith('gut_gut.svg')) {
        document.title = 'Das Gut^2 Buch';
        document.body.querySelector('main > header > h1').innerHTML = '<a href="https://bitbucket.org/guan-jing-ren/gut/src">Das Gut<sup>2</sup> Buch</a>';
        toctree.src = 'gut_dependencies.svg';
    }

    if (window != window.parent) return;

    var script = document.head.outerHTML;
    var mainsrc = document.getElementsByTagName('header')[0].outerHTML + document.getElementsByTagName('main')[0].outerHTML;
    srcdoc = '<!doctype html><html lang="en-AU">' + script + mainsrc + '</html>';
    document.getElementById('colcount').addEventListener('change', columnChanged);
    columnChanged({ target: { value: 1 } });
    resizeOnInnerImg(document);
}

window.parent.addEventListener('load', setup);
